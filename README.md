# PATTERNS #
### Structural: ###
	(Y[cache], B) Flyweight BaseObject.SystemComponents
		      InputGameInterface.gameControllers

	(B) Composite ObjectManager.GameObjects
	
	(L) Facade GameEngine(UpdateThread, DrawThread)
	
	Adapter IGameView.Draw(SurfaceGameView); client: DrawThread

	Proxy GameFragment.ViewOnTouch
	
	(B) Bridge InputGameInterface<->ITouchFilter ???
	
### Behavioral: ###
	(L) Strategy LevelSystem.GenerateLevel(IWorldGenerator)

	(K) Template GameController.Update, YamgThread(DrawThread, UpdateThread)
	
	(B) Mediator (GameObject.Retrieve() GameComponent.Notify())
	
	
### Creational: ###
	(K) Factory GameObjectFactory.Spawn

	Builder GameObjectFactory.SpawPlayer

-----------------
???
GameEngine.SetGameOptions