using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using YAMG.App.Fragments;
using YAMG.App.Fragments.Abstract;

namespace YAMG.App
{
    /// <summary>
    /// Yet Another Maze Game
    /// 
    /// It's good to minimize number of activities in android app.
    /// This activity a container for fragments.
    /// Use @+id/container to load the fragments into this activity.
    /// </summary>
    [Activity(Label = "YAMG",
        MainLauncher = true,
        ScreenOrientation = ScreenOrientation.SensorPortrait)]
    public class YamgActivity : Activity
    {
        private const string TagFragment = "content";

        #region Activity

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_yamg);
            if (savedInstanceState == null)
            {
                FragmentManager
                    .BeginTransaction()
                    .Add(Resource.Id.container, new MainMenuFragment(), TagFragment)
                    .Commit();
            }
        }

        public override void OnBackPressed()
        {
            var fragment = FragmentManager.FindFragmentByTag(TagFragment) as BaseFragment;
            if (fragment == null || !fragment.OnBackPressed())
            {
                base.OnBackPressed();
            }
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);
            if (hasFocus)
            {
                SystemUiFlags uiOptions;
                if (Build.VERSION.SdkInt < BuildVersionCodes.Kitkat)
                {
                    uiOptions = SystemUiFlags.LayoutStable
                            | SystemUiFlags.LayoutFullscreen
                            | SystemUiFlags.Fullscreen
                            | SystemUiFlags.LowProfile;
                }
                else
                {
                    uiOptions = SystemUiFlags.LayoutStable
                            | SystemUiFlags.LayoutFullscreen
                            | SystemUiFlags.Fullscreen
                            | SystemUiFlags.HideNavigation
                            | SystemUiFlags.LayoutHideNavigation
                            | SystemUiFlags.ImmersiveSticky;

                }
                Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            }
        }

        #endregion

        public void StartGame()
        {
            // Navigate the the game fragment, which makes the start automatically
            NavigateToFragment(new GameFragment());
        }

        public void NavigateBack()
        {
            // Do a push on the navigation history
            //base.OnBackPressed();
            FragmentManager.PopBackStack();
        }

        public void NavigateToFragment(Fragment destinationFragment)
        {
            FragmentManager
              .BeginTransaction()
              .Replace(Resource.Id.container, destinationFragment, TagFragment)
              .AddToBackStack(null)
              .Commit();
        }
    }
}