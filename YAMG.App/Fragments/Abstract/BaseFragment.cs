using System;
using Android.App;
using Android.OS;
using Android.Views;

namespace YAMG.App.Fragments.Abstract
{
    public abstract class BaseFragment : Fragment
    {
        /// <summary>
        /// Returns YamgActivity to which current fragment is attached.
        /// </summary>
        protected YamgActivity YamgActivity => (YamgActivity)Activity;

        #region Fragment

        private readonly object syncLock = new object();

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            view.ViewTreeObserver.GlobalLayout += ObserverOnGlobalLayout;
        }

        private void ObserverOnGlobalLayout(object sender, EventArgs eventArgs)
        {
            lock (syncLock)
            {
                // To avoid being called multiple times and,
                // therefore, running init logic multiple times, 
                // we need to remove the listener as soon as it is called
                var vto = sender as ViewTreeObserver;
                if (vto != null && vto.IsAlive)
                    vto.GlobalLayout -= ObserverOnGlobalLayout;

                OnLayoutCompleted();
            }
        }

        #endregion

        public virtual bool OnBackPressed()
        {
            return false;
        }

        /// <summary>
        /// Override in child class to run init logic
        /// after view is fully created and measured.
        /// </summary>
        protected virtual void OnLayoutCompleted() { }
    }
}