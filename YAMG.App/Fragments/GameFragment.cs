using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Views;
using Android.Widget;
using Java.Lang;
using YAMG.App.Fragments.Abstract;
using YAMG.Engine.Drawing;
using YAMG.Engine.Input;
using YAMG.Game;
using Enum = System.Enum;
using Environment = System.Environment;

namespace YAMG.App.Fragments
{
    public class GameFragment : BaseFragment
    {
        private GameEngine gameEngine;
        private long lastTouchTime = 0L;

        #region Fragment

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.fragment_game, container, false);
            view.Touch += ViewOnTouch;
            return view;
        }

        //TODO Pattern proxy
        private void ViewOnTouch(object sender, View.TouchEventArgs touchEventArgs)
        {
            // Process touch event if game is running
            // and sleep for 32 milis to prevent ui flooding.
            // No need to get input every milisecond
            // p.s. 32 ~ 1000ms / 30fps
            if (!gameEngine.IsPaused)
            {
                gameEngine.OnTouchEvent(touchEventArgs.Event);
                //Log.Info("TOUCH", $"{touchEventArgs.Event.GetX()}, {touchEventArgs.Event.GetY()}");

                var time = Environment.TickCount;
                if (touchEventArgs.Event.Action == MotionEventActions.Move
                    && time - lastTouchTime < 32)
                {
                    try
                    {
                        Thread.Sleep(32);
                    }
                    catch (InterruptedException)
                    {
                        // Ignore.
                    }
                }
                lastTouchTime = time;
            }

            // tell 'em we are good
            touchEventArgs.Handled = true;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            view.FindViewById<Button>(Resource.Id.btnPlayPause).Click +=
                (sender, args) => PauseGameAndShowPauseDialog();
        }

        public override void OnPause()
        {
            base.OnPause();
            if (gameEngine.IsRunning)
            {
                PauseGameAndShowPauseDialog();
            }
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            gameEngine.StopGame();
        }

        #endregion

        protected override void OnLayoutCompleted()
        {
            var gameView = (IGameView)View.FindViewById(Resource.Id.gameView);
            gameEngine = new GameEngine(Activity, gameView);

            var prefs = PreferenceManager.GetDefaultSharedPreferences(Activity);


            var gameControllerValue = prefs.GetString(Activity.Resources.GetString(Resource.String.controllerPreferenceKey), "0");
            var gameController = (InputGameIntefrace.GameControllerType)Enum.Parse(typeof(InputGameIntefrace.GameControllerType), gameControllerValue);

            gameEngine.SetGameOptions(gameController);

            gameEngine.StartGame();
            gameView.PostInvalidate();
        }

        public override bool OnBackPressed()
        {
            if (gameEngine.IsRunning && !gameEngine.IsPaused)
            {
                PauseGameAndShowPauseDialog();
                return true;
            }
            return base.OnBackPressed();
        }

        private void PauseGameAndShowPauseDialog()
        {
            if (gameEngine.IsPaused)
                return;

            gameEngine.PauseGame();

            new AlertDialog.Builder(Activity)
                .SetTitle(Resource.String.pauseDialogTitle)
                .SetMessage(Resource.String.pauseDialogMessage)
                .SetPositiveButton(Resource.String.resume, (sender, args) =>
                {
                    gameEngine.ResumeGame();
                })
                .SetNegativeButton(Resource.String.stop, (sender, args) =>
                {
                    gameEngine.StopGame();
                    YamgActivity.NavigateBack();
                })
                .SetCancelable(true)
                .SetOnCancelListener(new PauseDialogCancel(gameEngine))
                .Create()
                .Show();
        }

        class PauseDialogCancel : Object, IDialogInterfaceOnCancelListener
        {
            private readonly GameEngine engine;

            public PauseDialogCancel(GameEngine engine)
            {
                this.engine = engine;
            }

            public void OnCancel(IDialogInterface dialog)
            {
                engine.ResumeGame();
            }
        }
    }
}