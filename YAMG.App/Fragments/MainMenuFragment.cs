using Android.OS;
using Android.Views;
using Android.Widget;
using YAMG.App.Fragments.Abstract;

namespace YAMG.App.Fragments
{
    public class MainMenuFragment : BaseFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            return inflater.Inflate(Resource.Layout.fragment_main_menu, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            view.FindViewById<Button>(Resource.Id.btnStart).Click +=
                (s, e) => YamgActivity.StartGame();

            view.FindViewById<Button>(Resource.Id.btnSettings).Click +=
                (s, e) => YamgActivity.NavigateToFragment(new PreferencesFragment());
        }
    }
}