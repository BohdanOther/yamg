using Android.OS;
using Android.Preferences;

namespace YAMG.App.Fragments
{
    public class PreferencesFragment : PreferenceFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Load the preferences from an XML resource
            AddPreferencesFromResource(Resource.Xml.preferences);
        }
    }
}