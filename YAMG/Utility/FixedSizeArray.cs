﻿using Android.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace YAMG.Utility
{

    /// <summary>
    /// Fixed size array with neat functionallity
    /// taken from Unity engine
    /// </summary>
    public class FixedSizeArray<T> where T : class
    {
        public T[] Content { get; }

        /// <summary>
        /// Returns the number of objects in the array.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Returns the maximum number of objects that can be inserted inot this array.
        /// </summary>
        public int Capacity => Content.Length;

        public ISorter<T> Sorter { get; set; }
        public IComparer<T> Comparator { get; set; }


        private static readonly int LinearSearchCutoff = 16;

        private bool isSorted;


        public FixedSizeArray(int size)
        {
            Debug.Assert(size > 0);

            Content = new T[size];
            Count = 0;
            isSorted = false;

            Sorter = new StandartSorter<T>();
        }

        public FixedSizeArray(int size, IComparer<T> comparator)
        {
            Debug.Assert(size > 0);

            Content = new T[size];
            Count = 0;
            Comparator = comparator;
            isSorted = false;

            Sorter = new StandartSorter<T>();
        }

        /// <summary>
        /// Inserts a new object into the array.  If the array is full, an assert is thrown and the
        /// object is ignored.</summary>
        public void Add(T item)
        {
            Debug.Assert(Count < Content.Length, "Array exhausted!");
            if (Count < Content.Length)
            {
                Content[Count] = item;
                isSorted = false;
                Count++;
            }
        }

        /// <summary>
        /// Searches for an object and removes it from the array if it is found.  Other indexes in the
        /// array are shifted up to fill the space left by the removed object.  Note that if
        /// ignoreComparator is set to true, a linear search of object references will be performed.
        /// Otherwise, the comparator set on this array(if any) will be used to find the object.
        /// </summary>
        public void Remove(T item, bool ignoreComparator)
        {
            int index = Find(item, ignoreComparator);
            if (index != -1)
                Remove(index);
        }

        /// <summary>
        /// Removes the specified index from the array.  Subsequent entries in the array are shifted up
        /// to fill the space.
        /// </summary>
        public void Remove(int index)
        {
            Debug.Assert(index < Count);
            if (index < Count)
            {
                for (var x = index; x < Count; x++)
                {
                    if (x + 1 < Content.Length && x + 1 < Count)
                    {
                        Content[x] = Content[x + 1];
                    }
                    else
                    {
                        Content[x] = null;
                    }
                }
                Count--;
            }
        }

        /// <summary>
        /// Removes the last element in the array and returns it.  
        /// This method is faster than calling Remove(count -1);
        /// </summary>
        /// <returns>The contents of the last element in the array.</returns>
        public T RemoveLast()
        {
            T @object = null;
            if (Count > 0)
            {
                @object = Content[Count - 1];
                Content[Count - 1] = null;
                Count--;
            }
            return @object;
        }

        /// <summary>
        /// Swaps the element at the passed index with the element at the end of the array.  When
        /// followed by removeLast(), this is useful for quickly removing array elements.
        /// </summary>
        /// <param name="index"></param>
        public void SwapWithLast(int index)
        {
            if (Count > 0 && index < Count - 1)
            {
                T @object = Content[Count - 1];
                Content[Count - 1] = Content[index];
                Content[index] = @object;
                isSorted = false;
            }
        }

        /// <summary>
        /// Clears the contents of the array, releasing all references to objects it contains and
        /// setting its count to zero.
        /// </summary>
        public void Clear()
        {
            for (var x = 0; x < Count; x++)
            {
                Content[x] = null;
            }
            Count = 0;
            isSorted = false;
        }

        /// <summary>
        /// Searches the array for the specified object.  If the array has been sorted with sort(),
        /// and if no other order-changing events have occurred since the sort (e.g. add()), a
        /// binary search will be performed.  If a comparator has been specified with setComparator(),
        /// it will be used to perform the search.  If not, the default comparator for the object type
        /// will be used.  If the array is unsorted, a linear search is performed.
        /// Note that if ignoreComparator is set to true, a linear search of object references will be
        /// performed. Otherwise, the comparator set on this array (if any) will be used to find the
        /// object.
        /// </summary>
        /// <param name="object">The object to search for.</param>
        /// <param name="ignoreComparator">Ignore comparator</param>
        /// <returns>The index of the object in the array, or -1 if the object is not found.</returns>
        public int Find(T @object, bool ignoreComparator)
        {
            int index = -1;
            int count = Count;
            bool sorted = isSorted;
            var comparator = Comparator;
            var contents = this.Content;

            if (sorted && !ignoreComparator && count > LinearSearchCutoff)
            {
                index = comparator != null
                    ? Array.BinarySearch(contents, @object, comparator)
                    : Array.BinarySearch(contents, @object);

                // Array.BinarySearch returns a negative insertion index if the object isn't found,
                // but we just want a boolean.
                if (index < 0)
                    index = -1;
            }
            else
            {
                // unsorted, linear search
                if (comparator != null && !ignoreComparator)
                {
                    for (var x = 0; x < count; x++)
                    {
                        int result = comparator.Compare(contents[x], @object);
                        if (result == 0)
                        {
                            index = x;
                            break;
                        }

                        if (result > 0 && sorted)
                            // we've passed the object, early out
                            break;
                    }
                }
                else
                {
                    for (var x = 0; x < count; x++)
                    {
                        if (contents[x].Equals(@object))
                        {
                            index = x;
                            break;
                        }
                    }
                }
            }
            return index;
        }

        /// <summary>
        /// Sorts the array.  If the array is already sorted, no work will be performed unless
        /// the forceResort parameter is set to true.  If a comparator has been specified with
        /// setComparator(), it will be used for the sort; otherwise the object's natural ordering will
        /// be used.
        /// </summary>
        /// <param name="forceResort">If set to true, the array will be resorted even if the order of the
        /// objects in the array has not changed since the last sort.</param>
        public void Sort(bool forceResort)
        {
            if (!isSorted || forceResort)
            {
                if (Comparator != null)
                {
                    Sorter.Sort(Content, Count, Comparator);
                }
                else
                {
                    Log.Debug("FixedSizeArray", "No comparator specified for this type, using Arrays.sort().");
                    Array.Sort(Content, 0, Count);
                }
                isSorted = true;
            }
        }

        public T this[int index]
        {
            get
            {
                Debug.Assert(index < Count);
                T result = null;
                if (index < Count && index >= 0)
                    result = Content[index];

                return result;
            }
            set
            {
                Debug.Assert(index < Count);
                if (index < Count)
                    Content[index] = value;
            }
        }
    }
}
