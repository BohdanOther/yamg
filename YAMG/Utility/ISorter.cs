﻿using System.Collections.Generic;

namespace YAMG.Utility
{
    public interface ISorter<T>
    {
        void Sort(T[] array, int count, IComparer<T> comparator);
    }
}
