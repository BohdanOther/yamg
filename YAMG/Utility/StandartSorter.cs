﻿using System;
using System.Collections.Generic;

namespace YAMG.Utility
{
    public class StandartSorter<T>: ISorter<T>
    {
        public void Sort(T[] array, int count, IComparer<T> comparator)
        {
            Array.Sort(array, 0, count, comparator);
        }
    }
}
