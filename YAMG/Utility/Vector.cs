﻿using System;

namespace YAMG.Utility
{
    /// <summary>
    /// Vector class for basic vector math.
    /// </summary>
    public sealed class Vector
    {
        public float X { get; set; }
        public float Y { get; set; }

        #region Constructors

        public Vector()
        {
            Set(0, 0);
        }

        public Vector(float x, float y)
        {
            Set(x, y);
        }

        public Vector(Vector other)
        {
            Set(other);
        }

        #endregion

        #region Fluent API

        public Vector Add(Vector other)

        {
            X += other.X;
            Y += other.Y;

            return this;
        }
        public Vector Add(float otherX, float otherY)
        {
            X += otherX;
            Y += otherY;

            return this;
        }

        public Vector Subtract(Vector other)
        {
            X -= other.X;
            Y -= other.Y;

            return this;
        }
        public Vector Subtract(float otherX, float otherY)
        {
            X -= otherX;
            Y -= otherY;
            return this;
        }

        public Vector Multiply(float magnitude)
        {
            X *= magnitude;
            Y *= magnitude;

            return this;
        }
        public Vector Multiply(Vector other)
        {
            X *= other.X;
            Y *= other.Y;

            return this;
        }

        public Vector Divide(float magnitude)
        {
            if (Math.Abs(magnitude) > 0.0001)
            {
                X /= magnitude;
                Y /= magnitude;
            }

            return this;
        }

        public Vector Set(Vector other)
        {
            X = other.X;
            Y = other.Y;

            return this;
        }
        public Vector Set(float xValue, float yValue)
        {
            X = xValue;
            Y = yValue;

            return this;
        }

        public Vector Normalize()
        {
            float magnitude = Length();

            if (Math.Abs(magnitude) > 0.0001)
            {
                X /= magnitude;
                Y /= magnitude;
            }

            return this;
        }

        public Vector Zero()
        {
            Set(0.0f, 0.0f);

            return this;
        }

        #endregion

        public float Dot(Vector other)
        {
            return (X * other.X) + (Y * other.Y);
        }

        public float Length()
        {
            return (float)Math.Sqrt(Length2());
        }
        public float Length2()
        {
            return (X * X) + (Y * Y);
        }

        public float Distance(Vector other)
        {
            return (float)Math.Sqrt(Distance2(other));
        }
        public float Distance2(Vector other)
        {
            float dX = X - other.X;
            float dY = Y - other.Y;
            return (dX * dX) + (dY * dY);
        }
    }
}
