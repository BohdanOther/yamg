﻿using YAMG.Engine.Core;

namespace YAMG.Game.Controllers
{
    /// <summary>
    /// Fixed controller has fixed center. Pretty straightforward...
    /// </summary>
    public class FixedController : GameController
    {
        public override void Update(BaseObject parent)
        {
            var input = SystemComponents.InputGameIntefrace;

            Center.Set(input.FixedControllerPosition);

            base.Update(this);
        }
    }
}
