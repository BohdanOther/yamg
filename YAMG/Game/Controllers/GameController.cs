﻿using YAMG.Engine.Core;
using YAMG.Engine.Input;
using YAMG.Utility;

namespace YAMG.Game.Controllers
{
    /// <summary>
    /// Base controller, that
    ///  - exposes DPad - raw user directional input
    ///  - Center - DPad position
    /// 
    /// DPad values are calculated as normalized delta offset of input touch form center
    /// </summary>
    public abstract class GameController : BaseObject
    {
        /// <summary>
        /// Base input source in yamg game.
        /// </summary>
        /// <remarks>
        /// Looks like pointing direction is all we need.
        /// But if we need attack button, or some 'use item' button
        /// just add 
        ///     InputButton attack;
        /// and trigger that button from your controller implementation. 
        /// </remarks>
        public InputXY DPad { get; }

        /// <summary>
        /// DPad center from where we calculate delta offset
        ///     delta = touch - center
        /// Than from delta we extract velocity or what we need.
        /// </summary>
        public Vector Center { get; protected set; }

        // normalized delta offset from DPad center
        protected Vector Delta;

        protected GameController()
        {
            Delta = new Vector();
            Center = new Vector();
            DPad = new InputXY();
        }

        ///TODO Pattern template method
        /// <summary>
        /// Base update method, that takes set Center and calculates
        /// Dpad raw inputs as delta = inputPoint - centerPoint.
        /// 
        /// Delta is normalized to vector in one-unit circle.
        ///
        /// </summary>
        /// <param name="parent"></param>
        public override void Update(BaseObject parent)
        {
            var input = SystemComponents.InputGameIntefrace;
            var touch = SystemComponents.InputGameIntefrace.Touch;

            if (touch.IsPressed)
            {
                var dx = touch.X - Center.X;
                var dy = touch.Y - Center.Y;

                Delta.Set(dx, dy);

                if (Delta.Length2() < input.DPadThreshold * input.DPadThreshold)
                {
                    Delta.Zero();
                    DPad.Release();
                    return;
                }

                if (Delta.Length2() < input.DPadRadius * input.DPadRadius)
                {
                    Delta.Divide(input.DPadRadius);
                }
                else
                {
                    Delta.Normalize();
                }

                DPad.Press(Delta.X, Delta.Y);
            }
            else
            {
                DPad.Release();
            }
        }

        public override void Reset()
        {
            Delta.Zero();
            Center.Zero();
            DPad.Reset();
        }
    }
}
