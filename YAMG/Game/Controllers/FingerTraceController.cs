﻿using YAMG.Engine.Core;

namespace YAMG.Game.Controllers
{
    /// <summary>
    /// Trace controller has camera target as a center.
    /// </summary>
    public class FingerTraceController : GameController
    {
        public override void Update(BaseObject parent)
        {
            var target = SystemComponents.Camera.Target;
            if (target == null)
                return;

            Center.Set(target.CenterX, target.CenterY);

            base.Update(this);
        }
    }
}

