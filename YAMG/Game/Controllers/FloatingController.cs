﻿using YAMG.Engine.Core;

namespace YAMG.Game.Controllers
{
    /// <summary>
    /// Floating controller makes a first touch to be its relative center.
    /// When touch is released - center will change.
    /// </summary>
    public class FloatingController : GameController
    {
        private bool isPressed = false;

        public override void Update(BaseObject parent)
        {
            var touch = SystemComponents.InputGameIntefrace.Touch;

            if (touch.IsPressed && !isPressed)
            {
                isPressed = true;
                Center.Set(touch.X, touch.Y);
            }

            isPressed = touch.IsPressed;

            base.Update(this);
        }
    }
}
