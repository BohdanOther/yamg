﻿namespace YAMG.Game.Constants
{
    public class GameConstants
    {
        public const int TileWidth = 32;
        public const int TileHeight = 32;

        public const int PlayerWidth = 28;
        public const int PlayerHeight = 28;
    }
}
