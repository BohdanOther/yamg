﻿using YAMG.Engine;
using YAMG.Engine.Core;
using YAMG.Engine.System.Collision;
using YAMG.Engine.World;
using YAMG.Game.Components;
using YAMG.Game.Constants;

namespace YAMG.Game.Utility
{
    public class GameObjectFactory : BaseObject
    {
        // TODO: add index to type extension
        // add spawn by type method
        public enum GameObjectType
        {
            Invalid = 0,

            TileWall = 1,
            Enter = 2,
            Exit = 3,

            Player = 4
        }
        public override void Reset() { }

        // TODO Pattern factory method
        public GameObject Spawn(GameObjectType objectType, float x, float y)
        {
            switch (objectType)
            {
                case GameObjectType.Player: return SpawnPlayer(x, y);
                case GameObjectType.TileWall: return SpawnTileWall(x, y);
                case GameObjectType.Enter: return SpawnTileEnter(x, y);
                case GameObjectType.Exit: return SpawnTileExit(x, y);
                default: return null;
            }
        }

        public void SpawnFromWorld(TiledWorld world, int tileWidth, int tileHeight)
        {
            var gameRoot = SystemComponents.GameRoot;
            if (gameRoot == null)
                return;

            // Walk the world and spawn objects based on tile indexes.
            for (var y = 0; y < world.Height; y++)
            {
                for (var x = 0; x < world.Width; x++)
                {
                    int index = world[y, x];
                    if (index == 0)
                        continue;

                    var type = (GameObjectType)index; // todo: dangerous
                    if (type == GameObjectType.Invalid)
                        continue;

                    float worldX = x * tileWidth;
                    float worldY = y * tileHeight;
                    var gameObject = Spawn(type, worldX, worldY);

                    if (gameObject != null)
                    {
                        if (gameObject.Heigth < tileHeight)
                        {
                            // make sure small objects are vertically centered 
                            // in their tile.
                            gameObject.Position.Y += (tileHeight - gameObject.Heigth) / 2.0f;
                        }
                        if (gameObject.Width < tileWidth)
                        {
                            gameObject.Position.X += (tileWidth - gameObject.Width) / 2.0f;
                        }
                        else if (gameObject.Width > tileWidth)
                        {
                            gameObject.Position.X -= (gameObject.Width - tileWidth) / 2.0f;
                        }

                        gameRoot.Add(gameObject);

                        if (type == GameObjectType.Player)
                        {
                            //gameObjectManager.Player = gameObject;
                        }
                    }
                }
            }
        }

        public GameObject[][] SpawnGameWorld(TiledWorld world, int tileWidth, int tileHeight)
        {
            var _world = new GameObject[world.Height][];

            for (var y = 0; y < world.Height; y++)
            {
                _world[y] = new GameObject[world.Width];
                for (var x = 0; x < world.Width; x++)
                {
                    int index = world[y, x];
                    if (index == 0)
                        continue;

                    var type = (GameObjectType)index;
                    if (type == GameObjectType.Invalid)
                        continue;

                    float worldX = x * tileWidth;
                    float worldY = y * tileHeight;
                    var gameObject = Spawn(type, worldX, worldY);

                    _world[y][x] = gameObject;
                }
            }

            return _world;
        }

        // TODO Builder pattern
        #region Spawn Game Object Type

        public GameObject SpawnPlayer(float x, float y)
        {
            var gameObject = new GameObject
            {
                Width = GameConstants.PlayerWidth,
                Heigth = GameConstants.PlayerHeight
            };

            gameObject.Position.Set(x, y);
            gameObject.Acceleration.Set(3, 3);

            var player = new PlayerComponent();
            var movement = new MovementComponent();

            var aabb = new BoundingBox(
                x, y,
                gameObject.Width, gameObject.Heigth,
                0, 0);
            var collision = new WorldCollisionComponent(aabb);

            gameObject.Add(movement);
            gameObject.Add(player);
            gameObject.Add(collision);


            var render = new PlayerRenderComponent();
            gameObject.Add(render);

            var camera = SystemComponents.Camera;
            if (camera != null)
            {
                camera.Target = gameObject;
            }

            return gameObject;
        }

        public GameObject SpawnTileWall(float x, float y)
        {
            var gameObject = new GameObject
            {
                Width = GameConstants.TileWidth,
                Heigth = GameConstants.TileHeight
            };

            gameObject.Position.Set(x, y);

            var tileWall = new TileComponent(
                (int)gameObject.CenterX / GameConstants.TileWidth,
                (int)gameObject.CenterY / GameConstants.TileHeight
            );
            gameObject.Add(tileWall);
            gameObject.Add(new TileRenderComponent());

            return gameObject;
        }

        public GameObject SpawnTileEnter(float x, float y)
        {
            var gameObject = new GameObject
            {
                Width = GameConstants.TileWidth,
                Heigth = GameConstants.TileHeight
            };

            gameObject.Position.Set(x, y);

            //var tileWall = new TileComponent(
            //    (int)gameObject.CenterX / GameConstants.TileWidth,
            //    (int)gameObject.CenterY / GameConstants.PlayerHeight
            //);
            //gameObject.Add(tileWall);

            return gameObject;
        }

        public GameObject SpawnTileExit(float x, float y)
        {
            var gameObject = new GameObject
            {
                Width = GameConstants.TileWidth,
                Heigth = GameConstants.TileHeight
            };

            gameObject.Position.Set(x, y);

            //var tileWall = new TileComponent(
            //    (int)gameObject.CenterX / GameConstants.TileWidth,
            //    (int)gameObject.CenterY / GameConstants.PlayerHeight
            //);
            //gameObject.Add(tileWall);

            return gameObject;
        }

        #endregion
    }
}
