﻿using Android.Graphics;
using YAMG.Engine;
using YAMG.Engine.Core;
using YAMG.Game.Constants;
using YAMG.Utility;

namespace YAMG.Game.Components
{
    /// <summary>
    /// Add some king of DrawableComponent
    /// as temporary hack for nice render system
    /// </summary>
    public sealed class PlayerRenderComponent : GameComponent
    {
        private Vector pos;
        private Vector playerSize;

        private Paint playerPaint;

        public PlayerRenderComponent() : base(ComponentPhases.Draw)
        {
            pos = new Vector();     
            playerSize = new Vector();

            playerPaint = new Paint
            {
                Color = new Color(198, 40, 40)
            };

            Reset();
        }

        public override void Reset()
        {
            playerSize.Set(GameConstants.PlayerWidth, GameConstants.PlayerHeight);
        }

        public override void Update(BaseObject parent)
        {
            var gameObject = (GameObject)parent;

            pos.Set(gameObject.Position);
        }

        public override void Render(Canvas canvas)
        {
            // draw player
            canvas.DrawRect(pos.X, pos.Y, pos.X + playerSize.X, pos.Y + playerSize.Y, playerPaint);
        }
    }
}
