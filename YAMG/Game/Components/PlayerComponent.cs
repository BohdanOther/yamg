﻿using YAMG.Engine;
using YAMG.Engine.Core;

namespace YAMG.Game.Components
{
    public class PlayerComponent : GameComponent
    {
        // TODO: PATTERN State automate
        public PlayerComponent() : base(ComponentPhases.Think) { }

        public override void Update(BaseObject parent)
        {
            var dpad = SystemComponents.GameController.DPad;
            var gameObject = (GameObject)parent;

            gameObject.Velocity.Set(dpad.X, dpad.Y);
        }
    }
}
