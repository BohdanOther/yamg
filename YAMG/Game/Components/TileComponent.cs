﻿using YAMG.Engine;
using YAMG.Engine.Core;

namespace YAMG.Game.Components
{
    public class TileComponent : GameComponent
    {

        private readonly int gridX;
        private readonly int gridY;


        public TileComponent(int gridX, int gridY) : base(ComponentPhases.Physics)
        {
            this.gridX = gridX;
            this.gridY = gridY;
        }


        public override void Update(BaseObject parent)
        {
        
        }
    }
}
