﻿using YAMG.Engine;
using YAMG.Engine.Core;
using YAMG.Engine.System.Collision;
using YAMG.Utility;

namespace YAMG.Game.Components
{
    /// <summary>
    /// Handles collision against the world tiles.
    /// </summary>
    public sealed class WorldCollisionComponent : GameComponent
    {
        private readonly BoundingBox aabb;
        private readonly Vector previousPosition;
        private readonly Vector delta;

        public WorldCollisionComponent(BoundingBox aabb) : base(ComponentPhases.CollisionDetection)
        {
            this.aabb = aabb;

            previousPosition = new Vector();
            delta = new Vector();

            Reset();
        }

        public override void Reset()
        {
            previousPosition.Set(aabb.Position);
            delta.Zero();
        }

        public override void Update(BaseObject parent)
        {
            var collision = SystemComponents.CollisionSystem;
            if (collision == null)
                return;


            var gameObject = (GameObject)parent;
            aabb.MoveTo(gameObject.Position);

            // If collision ocurred
            // step back and test each delta move component separately
            // to allow wall sliding
            if (collision.TestBox(aabb) != null)
            {
                delta.Set(gameObject.Position)
                     .Subtract(previousPosition);

                aabb.MoveTo(previousPosition)
                    .Translate(delta.X, 0);
                var collider = collision.TestBox(aabb);
                if (collider != null)
                {
                    aabb.MoveTo(previousPosition);

                    previousPosition.X += gameObject.Velocity.X > 0
                        ? collider.Left - aabb.Right
                        : collider.Right - aabb.Left;
                }
                else
                {
                    previousPosition.X += delta.X;
                }

                aabb.MoveTo(previousPosition)
                    .Translate(0, delta.Y);
                collider = collision.TestBox(aabb);
                if (collider != null)
                {
                    aabb.MoveTo(previousPosition);

                    previousPosition.Y += gameObject.Velocity.Y > 0
                        ? collider.Top - aabb.Bottom
                        : collider.Bottom - aabb.Top;
                }
                else
                {
                    previousPosition.Y += delta.Y;
                }

                gameObject.Position.Set(previousPosition);
            }

            previousPosition.Set(gameObject.Position);
        }
    }
}
