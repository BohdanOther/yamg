﻿using YAMG.Engine;
using YAMG.Engine.Core;
using YAMG.Utility;

namespace YAMG.Game.Components
{
    /// <summary>
    /// A game component that implements velocity-based movement.
    /// </summary>
    public class MovementComponent : GameComponent
    {
        // If multiple game components were ever running in different threads,
        // this would need to be non-static.
        private static readonly Interpolator Interpolator = new Interpolator();

        public MovementComponent() : base(ComponentPhases.Movement) { }

        public override void Update(BaseObject parent)
        {
            var gameObject = (GameObject)parent;
 
            gameObject.Position.Add(
                gameObject.Velocity.X * gameObject.Acceleration.X,
                gameObject.Velocity.Y * gameObject.Acceleration.Y
            );


            // TODO: in future we can get neat movement using this
            //Interpolator.Set(gameObject.Velocity.X,
            //    gameObject.TargetVelocity.X,
            //    gameObject.Acceleration.X
            //);

            //float offsetX = Interpolator.Interpolate(0.016f);
            //float newX = gameObject.Position.X + offsetX;
            //float newVelocityX = Interpolator.Current;

            //Interpolator.Set(gameObject.Velocity.Y,
            //    gameObject.TargetVelocity.Y,
            //    gameObject.Acceleration.Y
            //);

            //float offsetY = Interpolator.Interpolate(0.016f);
            //float newY = gameObject.Position.Y + offsetY;
            //float newVelocityY = Interpolator.Current;


            //gameObject.Position.Set(newX, newY);
            //gameObject.Velocity.Set(newVelocityX, newVelocityY);
        }
    }
}
