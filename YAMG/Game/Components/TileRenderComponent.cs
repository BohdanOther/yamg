﻿using Android.Graphics;
using YAMG.Engine;
using YAMG.Engine.Core;
using YAMG.Engine.System.Fov;
using YAMG.Game.Constants;

namespace YAMG.Game.Components
{
    // TODO: add sprite component and render system
    // than renderer will just render sprite
    // and sprite properties will be managed by actual component
    // There will be no "SomeObject"RenderComponent
    public class TileRenderComponent : GameComponent
    {
        private const float FadeSpeed = 1.2f;

        private float renderWidth;
        private float renderHeight;
        private float renderX;
        private float renderY;

        private Paint tilePaint;


        public TileRenderComponent() : base(ComponentPhases.Draw)
        {
            tilePaint = new Paint
            {
                Color = Color.Beige
            };
        }

        public override void Update(BaseObject parent)
        {
            var fov = SystemComponents.FovSystem;
            if (fov == null)
                return;

            var gameObject = (GameObject)parent;
            int gridX = (int)gameObject.CenterX / GameConstants.TileWidth;
            int gridY = (int)gameObject.CenterY / GameConstants.TileHeight;

            var fovState = fov.LightMap.GetLight(gridX, gridY);

            if (fovState == LightTile.Dark)
            {
                renderWidth -= FadeSpeed;
                if (renderWidth < 8)
                    renderWidth = 8;

                renderHeight -= FadeSpeed;
                if (renderHeight < 8)
                    renderHeight = 8;
            }
            else
            {
                renderWidth += FadeSpeed;
                if (renderWidth > GameConstants.TileWidth)
                    renderWidth = GameConstants.TileWidth;

                renderHeight += FadeSpeed;
                if (renderHeight > GameConstants.TileHeight)
                    renderHeight = GameConstants.TileHeight;
            }

            renderX = gameObject.Position.X + (gameObject.Width - renderWidth) / 2f;
            renderY = gameObject.Position.Y + (gameObject.Heigth - renderHeight) / 2f;
        }

        public override void Render(Canvas canvas)
        {
            canvas.DrawRect(renderX, renderY, renderX + renderWidth, renderY + renderHeight, tilePaint);
        }
    }
}
