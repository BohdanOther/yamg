﻿using Android.App;
using Android.Content;
using Android.Views;
using YAMG.Engine;
using YAMG.Engine.Core;
using YAMG.Engine.Drawing;
using YAMG.Engine.Input;
using YAMG.Engine.System;
using YAMG.Engine.System.Collision;
using YAMG.Engine.System.Fov;
using YAMG.Engine.Threading;
using YAMG.Engine.Utility;
using YAMG.Engine.World.Generator;
using YAMG.Game.Utility;

namespace YAMG.Game
{
    public class GameEngine
    {
        public bool IsRunning => updateThread != null && updateThread.IsRunning;
        public bool IsPaused => updateThread != null && updateThread.IsPaused;


        private readonly DrawThread drawThread;
        private readonly UpdateThread updateThread;
        private readonly IGameView gameView;

        private GameRoot gameRoot;


        // probably will never need
        public Context Context => gameView.Context;
        public Activity YamgActivity { get; set; }


        public GameEngine(Activity activity, IGameView gameView)
        {
            YamgActivity = activity;
            this.gameView = gameView;

            // this is game size we want to see
            // on every device
            int defW = 360;
            int defH = 640;

            // this is real device size
            var width = gameView.Width - gameView.PaddingRight - gameView.PaddingLeft;
            var height = gameView.Height - gameView.PaddingTop - gameView.PaddingBottom;

            // lets calculate screen ratio on current device
            // lets say, it is 16:9, for example
            float ratio = (float)width / height;

            // out desired ratio is 640/360 = 3:2
            // since 16/9 != 3/2 we will adjust width to match device ratio
            // this will only happen on devices with different screen ratio
            defW = (int)(defH * ratio);

            BaseObject.SystemComponents.ContextParameters = new ContextParameters
            {
                ViewWidth = width,
                ViewHeight = height,

                GameWidth = defW,
                GameHeight = defH,

                // This numbers show in how many times our target screen
                // differs from actual size.
                // In renderer we will scale all drawings by this factor
                // thus we will have game look the same on all devices.
                ScaleX = (float)width / defW,
                ScaleY = (float)height / defH,
            };


            updateThread = new UpdateThread();
            drawThread = new DrawThread(gameView);

            Bootstrap();
        }

        private void Bootstrap()
        {
            var gameRoot = new GameRoot();

            var inputGameIntefrace = new InputGameIntefrace();
            BaseObject.SystemComponents.InputGameIntefrace = inputGameIntefrace;

            // get default controller if none chosen
            var gameController = BaseObject.SystemComponents.GameController ??
                inputGameIntefrace.SetGameController(InputGameIntefrace.GameControllerType.Trace);
            BaseObject.SystemComponents.GameController = gameController;

            var camera = new CameraSystem();
            BaseObject.SystemComponents.Camera = camera;

            var gameObjectFactory = new GameObjectFactory();
            BaseObject.SystemComponents.GameObjectFactory = gameObjectFactory;

            var collision = new CollisionSystem();
            BaseObject.SystemComponents.CollisionSystem = collision;

            var fov = new FovSystem();
            BaseObject.SystemComponents.FovSystem = fov;
            gameRoot.Add(fov);

            var hud = new HudSystem();
            BaseObject.SystemComponents.HudSystem = hud;
            gameRoot.Add(hud);

            gameRoot.Add(BaseObject.SystemComponents.GameController);
            gameRoot.Add(camera);


            this.gameRoot = gameRoot;
            updateThread.GameRoot = gameRoot;
            gameView.GameRoot = gameRoot;
            // temp hack
            BaseObject.SystemComponents.GameRoot = gameRoot;


            var levelSystem = new LevelSystem();
            BaseObject.SystemComponents.LevelSystem = levelSystem;
            levelSystem.GenerateLevel(18, 9, new StubGenerator(), gameRoot);
        }

        //TODO Pattern facade
        public void StartGame()
        {
            // Stop a game if it is running
            StopGame();

            updateThread.Start();
            drawThread.Start();
        }

        public void StopGame()
        {
            updateThread?.StopGame();
            drawThread?.StopGame();
        }

        public void PauseGame()
        {
            updateThread?.PauseGame();
            drawThread?.PauseGame();
        }

        public void ResumeGame()
        {
            updateThread?.ResumeGame();
            drawThread?.ResumeGame();
        }


        public void OnTouchEvent(MotionEvent e)
        {
            BaseObject.SystemComponents.InputGameIntefrace.OnTouchEvent(e);
        }


        // pass params objects later as game config
        // TODO Pattern decorator / bridge 
        public void SetGameOptions(InputGameIntefrace.GameControllerType gameControllerType)
        {
            gameRoot.Remove(BaseObject.SystemComponents.GameController);
            var ctrl = BaseObject.SystemComponents.InputGameIntefrace.SetGameController(gameControllerType);
            gameRoot.Add(ctrl);
        }
    }
}
