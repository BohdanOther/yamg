﻿using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using YAMG.Engine.Core;

namespace YAMG.Engine.Drawing
{
    public sealed class SurfaceGameView : SurfaceView, ISurfaceHolderCallback, IGameView
    {
        public GameRoot GameRoot { private get; set; }

        private bool isSurfaceReady;

        #region SurfaceView Constructor

        public SurfaceGameView(Context context) : base(context)
        {
            Holder.AddCallback(this);
        }

        public SurfaceGameView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Holder.AddCallback(this);
        }

        public SurfaceGameView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Holder.AddCallback(this);
        }

        #endregion

        //TODO Pattern adapter
        public void Draw()
        {
            if (!isSurfaceReady)
                return;

            var canvas = Holder.LockCanvas();
            if (canvas == null)
                return;
            canvas.DrawRGB(64, 64, 64);

            int savedState = canvas.Save();
            canvas.Scale(BaseObject.SystemComponents.ContextParameters.ScaleX, BaseObject.SystemComponents.ContextParameters.ScaleY);

            lock (GameRoot.GameObjects)
            {
                GameRoot.Render(canvas);
            }

            canvas.RestoreToCount(savedState);
            Holder.UnlockCanvasAndPost(canvas);
        }

        #region ISurfaceHolderCallback

        public void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
        {
        }

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            isSurfaceReady = true;
            // If game was paused and minimized,
            // after restore we'll see black screen and ui only;
            // this will render last game state
            Draw();
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            isSurfaceReady = false;
        }

        #endregion
    }
}
