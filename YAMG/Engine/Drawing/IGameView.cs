﻿using Android.Content;

namespace YAMG.Engine.Drawing
{
    public interface IGameView
    {
        GameRoot GameRoot { set; }

        int Height { get; }
        int Width { get; }

        int PaddingLeft { get; }
        int PaddingRight { get; }
        int PaddingTop { get; }
        int PaddingBottom { get; }

        Context Context { get; }

        void Draw();
        void PostInvalidate();
    }
}
