﻿namespace YAMG.Engine.Core
{
    /// <summary>
    /// A basic object that adds an execution phase. When PhasedObjects are combined with
    /// PhasedObjectManagers, objects within the manager will be updated by phase.
    /// </summary>
    public class PhasedObject : BaseObject
    {
        public int Phase { get; set; }

        public override void Reset() { }
    }
}
