﻿using Android.Graphics;
using YAMG.Engine.System;

namespace YAMG.Engine.Core
{
    /// <summary>
    /// Core object from witch every logical unit should derive
    /// to get access to system components registry.
    /// </summary>
    public abstract class BaseObject
    {
        // TODO: make this private and expose GetSystemComponent<T>, AddSystemComponent<T> methods
        // use Dictionary<Type, BaseObject> for fast lookup
        // TODO: Pattern: flyweight ?? singleton
        public static SystemRegistry SystemComponents = new SystemRegistry();

        /// <summary>
        /// Anything, that needs an update per frame, should override this method.
        /// </summary>
        /// <param name="parent">The parent of this object. May be null</param>
        public virtual void Update(BaseObject parent) { }

        public abstract void Reset();

        public virtual void Render(Canvas canvas) { }
    }
}
