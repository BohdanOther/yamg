﻿using Android.Graphics;
using YAMG.Utility;

namespace YAMG.Engine.Core
{
    public class ObjectManager : BaseObject
    {
        //TODO Composite pattern
        public FixedSizeArray<BaseObject> GameObjects => Objects;

        protected static int DefaultArraySize = 64;

        protected readonly FixedSizeArray<BaseObject> Objects;
        protected readonly FixedSizeArray<BaseObject> PendingAdditions;
        protected readonly FixedSizeArray<BaseObject> PendingRemovals;

        public ObjectManager() : this(DefaultArraySize) { }

        public ObjectManager(int arraySize)
        {
            Objects = new FixedSizeArray<BaseObject>(arraySize);
            PendingAdditions = new FixedSizeArray<BaseObject>(arraySize);
            PendingRemovals = new FixedSizeArray<BaseObject>(arraySize);
        }

        public override void Reset()
        {
            CommitUpdates();
            int count = Objects.Count;
            for (var i = 0; i < count; i++)
            {
                var baseObject = Objects[i];
                baseObject.Reset();
            }
        }

        public virtual void CommitUpdates()
        {
            lock (Objects)
            {
                int additionCount = PendingAdditions.Count;
                if (additionCount > 0)
                {
                    for (var i = 0; i < additionCount; i++)
                    {
                        var baseObject = PendingAdditions[i];
                        Objects.Add(baseObject);
                    }
                    PendingAdditions.Clear();
                }

                int removalCount = PendingRemovals.Count;
                if (removalCount > 0)
                {
                    for (var i = 0; i < removalCount; i++)
                    {
                        var baseObject = PendingRemovals[i];
                        Objects.Remove(baseObject, true);
                    }
                    PendingRemovals.Clear();
                }
            }
        }
        
        public override void Update(BaseObject parent)
        {
            CommitUpdates();
            int count = Objects.Count;
            for (int i = 0; i < count; i++)
            {
                var baseObject = Objects[i];
                baseObject.Update(this);
            }
        }

        public override void Render(Canvas canvas)
        {
            int count = Objects.Count;
            for (int i = 0; i < count; i++)
            {
                var baseObject = Objects[i];
                baseObject.Render(canvas);
            }
        }


        public virtual void Add(BaseObject obj)
        {
            PendingAdditions.Add(obj);
        }

        public virtual void Remove(BaseObject obj)
        {
            PendingRemovals.Add(obj);
        }

        public virtual void RemoveAll()
        {
            int count = Objects.Count;
            for (int i = 0; i < count; i++)
            {
                PendingRemovals.Add(Objects[i]);
            }
            PendingAdditions.Clear();
        }


        public T FindByClass<T>() where T : BaseObject
        {
            T obj = null;
            int count = Objects.Count;
            for (int i = 0; i < count; i++)
            {
                var currentObject = Objects[i];
                if (currentObject is T)
                {
                    obj = (T)currentObject;
                    break;
                }
            }
            return obj;
        }
    }
}
