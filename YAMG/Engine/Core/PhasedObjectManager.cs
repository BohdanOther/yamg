﻿using System.Collections.Generic;
using System.Diagnostics;

namespace YAMG.Engine.Core
{
    /// <summary>
    /// A derivation of ObjectManager that sorts its children if they are of type PhasedObject.
    /// Sorting is performed on add.
    /// </summary>
    public class PhasedObjectManager : ObjectManager
    {
        private static readonly PhasedObjectComparator phasedObjectComparator = new PhasedObjectComparator();

        private bool mDirty;
        private readonly PhasedObject mSearchDummy;  // A dummy object allocated up-front for searching by phase.

        public PhasedObjectManager() : base()
        {
            mDirty = false;
            GameObjects.Comparator = phasedObjectComparator;
            PendingAdditions.Comparator = phasedObjectComparator;
            mSearchDummy = new PhasedObject();
        }

        public PhasedObjectManager(int arraySize) : base(arraySize)
        {
            mDirty = false;
            GameObjects.Comparator = phasedObjectComparator;
            PendingAdditions.Comparator = phasedObjectComparator;
            mSearchDummy = new PhasedObject();
        }

        public override void CommitUpdates()
        {
            base.CommitUpdates();
            if (mDirty)
            {
                Objects.Sort(true);
                mDirty = false;
            }
        }

        public override void Add(BaseObject baseObject)
        {
            if (baseObject is PhasedObject)
            {
                base.Add(baseObject);
                mDirty = true;
            }
            else
            {
                // The only reason to restrict PhasedObjectManager to PhasedObjects is so that
                // the PhasedObjectComparator can assume all of its contents are PhasedObjects and
                // avoid calling instanceof every time.
                Debug.Assert(false, "Can't add a non-PhasedObject to a PhasedObjectManager!");
            }
        }

        public BaseObject Find(int phase)
        {
            mSearchDummy.Phase = phase;
            int index = Objects.Find(mSearchDummy, false);
            BaseObject result = null;
            if (index != -1)
            {
                result = Objects[index];
            }
            else
            {
                index = PendingAdditions.Find(mSearchDummy, false);
                if (index != -1)
                {
                    result = PendingAdditions[index];
                }
            }
            return result;
        }

        /// <summary>
        /// Custom comparator for phased objects.
        /// </summary>
        private class PhasedObjectComparator : IComparer<BaseObject>
        {
            public int Compare(BaseObject x, BaseObject y)
            {
                int result = 0;
                if (x != null && y != null)
                {
                    result = ((PhasedObject)x).Phase - ((PhasedObject)y).Phase;
                }
                else if (x == null && y != null)
                {
                    result = 1;
                }
                else if (y == null && x != null)
                {
                    result = -1;
                }

                return result;
            }
        }
    }
}
