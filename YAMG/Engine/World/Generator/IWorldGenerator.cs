﻿namespace YAMG.Engine.World.Generator
{
    public interface IWorldGenerator
    {
        /// <summary>
        /// World generator should return world matrix,
        /// where only these values are considered valid:
        ///     0: empty tile;
        ///     1: wall;
        ///     2: entrance (mantadoty);
        ///     3: exit (mantadory).
        /// 
        /// Generated world without entrance or exit
        /// is considered to be invalid.
        /// </summary>
        /// <param name="width">World width in tiles</param>
        /// <param name="height">Word height in tiles</param>
        /// <returns>Generated world data</returns>
        int[][] Generate(int width, int height);
    }
}