﻿using System;
using System.IO;

namespace YAMG.Engine.World
{
    public class TiledWorld
    {
        public int Width { get; }
        public int Height { get; }
        public int[][] Tiles { get; }

        public TiledWorld(int cols, int rows)
        {
            Tiles = new int[rows][];
            Height = rows;
            Width = cols;

            for (int y = 0; y < rows; y++)
            {
                Tiles[y] = new int[cols];
                for (int x = 0; x < cols; x++)
                {
                    Tiles[y][x] = -1;
                }
            }
        }

        public TiledWorld(StreamReader stream)
        {
            // ParseInput(stream);
        }

        public TiledWorld(int[][] tiles)
        {
            if (tiles?[0] == null)
                throw new ArgumentNullException(nameof(tiles));

            Height = tiles.GetLength(0);
            Width = tiles[0].Length;

            Tiles = tiles;
        }

        public int this[int y, int x]
        {
            get
            {
                int result = -1;
                if (x >= 0 && x < Width && y >= 0 && y < Height)
                {
                    result = Tiles[y][x];
                }
                return result;
            }
            set { Tiles[y][x] = value; }
        }
    }
}
