﻿using Android.Graphics;
using System;
using YAMG.Engine.Core;

namespace YAMG.Engine.World
{
    public class World : BaseObject
    {
        public GameObject[][] Tiles;

        private int[][] skips;

        private readonly int worldWidth;
        private readonly int worldHeight;

        private int updRad = 20;
        private int renRad = 20;

        public World(GameObject[][] tiles)
        {
            Tiles = tiles;
            worldWidth = tiles[0].Length;
            worldHeight = tiles.GetLength(0);

            CalculateSkips();
        }
        public override void Reset()
        {
        }

        public override void Update(BaseObject parent)
        {
            var cam = SystemComponents.Camera;

            int bottom = Math.Max(cam.TileY - updRad, 0);
            int top = Math.Min(cam.TileY + updRad + 1, worldHeight);
            int left = Math.Max(cam.TileX - updRad, 0);
            int right = Math.Min(cam.TileX + updRad + 1, worldWidth);

            for (int i = bottom; i < top; i++)
            {
                for (int j = left; j < right; j++)
                {
                    if (Tiles[i][j] != null)
                    {
                        Tiles[i][j].Update(this);
                    }
                    else
                    {
                        j += skips[i][j] - 1;
                    }
                }
            }
        }

        public override void Render(Canvas canvas)
        {
            var cam = SystemComponents.Camera;

            int bottom = Math.Max(cam.TileY - renRad, 0);
            int top = Math.Min(cam.TileY + renRad + 1, worldHeight);
            int left = Math.Max(cam.TileX - renRad, 0);
            int right = Math.Min(cam.TileX + renRad + 1, worldWidth);

            for (int i = bottom; i < top; i++)
            {
                for (int j = left; j < right; j++)
                {

                    if (Tiles[i][j] != null)
                    {
                        Tiles[i][j].Render(canvas);
                    }
                    else
                    {
                        j += skips[i][j] - 1;
                    }
                }
            }
        }

        // Neat easy trick to avoid redundant loops.
        // Assume we have world segment (where X is valid tile, _ is null)
        // X___X_X______X
        // this function will calculate for this:
        // 03210106543210
        // So in loops, if you found null tile,
        // just skip horizontally calculated number.
        private void CalculateSkips()
        {
            skips = new int[worldHeight][];
            int emptyTileCount = 0;
            for (int y = worldHeight - 1; y >= 0; y--)
            {
                skips[y] = new int[worldWidth];
                for (int x = worldWidth - 1; x >= 0; x--)
                {
                    if (Tiles[y][x] == null)
                    {
                        emptyTileCount++;
                        skips[y][x] = emptyTileCount;
                    }
                    else
                    {
                        emptyTileCount = 0;
                    }
                }
            }
        }
    }
}
