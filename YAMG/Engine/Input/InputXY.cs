﻿using System;
using YAMG.Utility;

namespace YAMG.Engine.Input
{
    public sealed class InputXY
    {
        public float X => xAxis.Magnitude;
        public float Y => yAxis.Magnitude;

        public Vector Magnitude
        {
            get
            {
                // TODO: use pool
                return new Vector(xAxis.Magnitude, yAxis.Magnitude);
            }
            set
            {
                xAxis.Magnitude = value.X;
                yAxis.Magnitude = value.Y;
            }
        }
        public bool IsPressed => xAxis.IsPressed || yAxis.IsPressed;
        public float LastPressedTime => Math.Max(xAxis.LastPressedTime, yAxis.LastPressedTime);

        private readonly InputButton xAxis;
        private readonly InputButton yAxis;

        public InputXY()
        {
            xAxis = new InputButton();
            yAxis = new InputButton();
        }
        public InputXY(InputButton xAxis, InputButton yAxis)
        {
            this.xAxis = xAxis;
            this.yAxis = yAxis;
        }

        public void Press(float x, float y)
        {
            xAxis.Press(x);
            yAxis.Press(y);
        }
        public void Release()
        {
            xAxis.Release();
            yAxis.Release();
        }
        public void ReleaseX()
        {
            xAxis.Release();
        }
        public void ReleaseY()
        {
            yAxis.Release();
        }

        public void SetVector(Vector vector)
        {
            vector.Set(xAxis.Magnitude, yAxis.Magnitude);
        }

        public void Reset()
        {
            xAxis.Reset();
            yAxis.Reset();
        }
    }
}
