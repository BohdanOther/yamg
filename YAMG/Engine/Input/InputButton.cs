﻿using System;

namespace YAMG.Engine.Input
{
    public class InputButton
    {
        public bool IsPressed { get; private set; }
        public float Magnitude
        {
            get { return IsPressed ? magnitude : 0.0f; }
            set { magnitude = value; }
        }
        public float PressedDuration => Environment.TickCount - pressedTime;
        public float LastPressedTime { get; private set; }

        private float pressedTime;
        private float magnitude;

        public void Press(float magnitude)
        {
            if (!IsPressed)
            {
                IsPressed = true;
                pressedTime = Environment.TickCount;
            }

            this.magnitude = magnitude;
            LastPressedTime = Environment.TickCount;
        }

        public void Release()
        {
            IsPressed = false;
        }

        public void Reset()
        {
            IsPressed = false;
            LastPressedTime = 0.0f;
            magnitude = 0.0f;
            pressedTime = 0.0f;
        }
    }
}
