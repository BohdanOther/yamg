﻿using Android.Views;
using System.Collections.Generic;
using YAMG.Engine.Core;
using YAMG.Game.Controllers;
using YAMG.Utility;

namespace YAMG.Engine.Input
{
    /// <summary>
    /// Layer between android raw input and GameController.
    /// 
    /// Holds touch data (sensor, tilt, orientation data if needed).
    /// Configures and sets game input controller
    /// </summary>
    /// 
    /// <remarks>
    /// Here you can keep fields like 
    ///     useSensorInput
    ///     useClickForAttack
    ///     setGameController
    /// etc.
    /// </remarks>
    /// 
    /// TODO: refactor; this class is kind of wierd
    /// this is supposed to be InputSystem attached to GameRoot
    public class InputGameIntefrace : BaseObject
    {
        public enum GameControllerType
        {
            /// <summary>
            /// Controller is fixed at some point and stays there.
            /// </summary>
            Fixed,

            /// <summary>
            /// Controller is relative to the camera target.
            /// </summary>
            Trace,

            /// <summary>
            /// Contoller appears where touch was pressed
            /// and stays there, being relative to the original input,
            /// until touch is released.
            /// </summary>
            Floating
        }

        // game controllers pool
        public GameControllerType CurrentGameControllerType { get; private set; }

        private readonly Dictionary<GameControllerType, GameController> gameControllers;

        // Why not good old MotionEvent ?
        // Well, main idea here to get away from android system ASAP.
        // This provides abstraction level, allowing, for instance,
        // to have iOS or trackball touch event handled some way, but use this same Touch field. 
        public InputXY Touch { get; }


        // TODO: get this 3 from settings fragment
        public float DPadRadius { get; set; } = 90;
        public float DPadThreshold { get; set; } = 32;
        public Vector FixedControllerPosition { get; set; } = new Vector(110, 520);

        //TODO Pattern flyweight
        public InputGameIntefrace()
        {
            Touch = new InputXY();

            gameControllers = new Dictionary<GameControllerType, GameController>
            {
                [GameControllerType.Fixed] = new FixedController(),
                [GameControllerType.Floating] = new FloatingController(),
                [GameControllerType.Trace] = new FingerTraceController()
            };
        }

        public GameController SetGameController(GameControllerType controllerType)
        {
            var controller = gameControllers[controllerType];
            SystemComponents.GameController = controller;
            CurrentGameControllerType = controllerType;

            return controller;
        }

        // TODO: adapter ?
        public void OnTouchEvent(MotionEvent motionEvent)
        {
            if (motionEvent.Action == MotionEventActions.Up)
            {
                Touch.Release();
            }
            else
            {
                Touch.Press(
                    motionEvent.RawX * (1f / SystemComponents.ContextParameters.ScaleX),
                    motionEvent.RawY * (1f / SystemComponents.ContextParameters.ScaleY)
                );
            }
        }

        public override void Reset()
        {
            Touch.Release();
        }
    }
}
