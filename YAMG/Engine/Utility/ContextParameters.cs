﻿namespace YAMG.Engine.Utility
{
    /// <summary>
    /// Contains global, typically constant parameters about the current operating context
    /// </summary>
    public class ContextParameters
    {
        public float GameWidth { get; set; }
        public float GameHeight { get; set; }

        public float ScaleX { get; set; }
        public float ScaleY { get; set; }

        public int ViewWidth { get; set; }
        public int ViewHeight { get; set; }
    }
}
