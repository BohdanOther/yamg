﻿using YAMG.Engine.Core;

namespace YAMG.Engine
{
    /// <summary>
    /// A game component implements a single feature of a game object.  
    /// Components are run once per frame when their parent object is active.
    /// Updating a game object is equivalent to updating all of its components.
    /// Note that a game object may contain more than one instance of the same type of component.
    /// </summary>
    // TODO: component pattern
    public class GameComponent : PhasedObject
    {
        // Defines high-level buckets within which components may choose to run.
        public enum ComponentPhases
        {
            /// <summary>
            /// Decisions are made
            /// </summary>
            Think,

            /// <summary>
            /// Impulse velocities are summed
            /// </summary>
            Physics,

            /// <summary>
            /// Inertia, friction, and bounce
            /// </summary>
            PostPhysics,

            /// <summary>
            /// Position is updated
            /// </summary>
            Movement,

            /// <summary>
            /// Intersections are detected
            /// </summary>
            CollisionDetection,

            /// <summary>
            /// Intersections are resolved
            /// </summary>
            CollisionResponse,

            /// <summary>
            /// Position is now final for the frame
            /// </summary>
            PostCollision,

            /// <summary>
            /// Animations are selected
            /// </summary>
            Animation,

            /// <summary>
            /// Drawing state is initialized
            /// </summary>
            PreDraw,

            /// <summary>
            /// Drawing commands are scheduled.
            /// </summary>
            Draw,

            /// <summary>
            /// Final cleanup before the next update
            /// </summary>
            FrameEnd
        }

        /// <summary>
        /// If this component is shared among other GameObjects
        /// </summary>
        public bool IsShared { get; set; }

        public GameComponent(ComponentPhases phase)
        {
            Phase = (int) phase;
        }
    }
}
