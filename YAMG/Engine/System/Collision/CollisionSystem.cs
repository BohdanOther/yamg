﻿using YAMG.Engine.Core;
using YAMG.Engine.World;

namespace YAMG.Engine.System.Collision
{
    public class CollisionSystem : BaseObject
    {
        private TiledWorld world;
        private int tileWidth;
        private int tileHeight;

        private BoundingBox[][] collisionWorld;

        public override void Reset()
        {
        }

        public void Initialize(TiledWorld world, int tileWidth, int tileHeight)
        {
            this.world = world;
            this.tileWidth = tileWidth;
            this.tileHeight = tileHeight;

            collisionWorld = new BoundingBox[world.Height][];
            for (int y = 0; y < world.Height; y++)
            {
                collisionWorld[y] = new BoundingBox[world.Width];
                for (int x = 0; x < world.Width; x++)
                {
                    float worldX = tileWidth * x;
                    float worldY = tileHeight * y;

                    // TODO: IsTileCollidable(int)
                    if (world[y, x] == 1)
                        collisionWorld[y][x] = new BoundingBox(
                            worldX, worldY,
                            tileWidth, tileHeight
                            );
                }
            }
        }

        public BoundingBox TestBox(BoundingBox boundingBox)
        {
            int tileX = (int)(boundingBox.Left + boundingBox.Size.X / 2f) / tileWidth;
            int tileY = (int)(boundingBox.Top + boundingBox.Size.Y / 2f) / tileHeight;

            for (int y = -1; y <= 1; y++)
            {
                for (int x = -1; x <= 1; x++)
                {
                    int testX = tileX + x;
                    int testY = tileY + y;
                    if (testX < 0
                        || testY < 0
                        || testX >= world.Width
                        || testY >= world.Height
                        || (x == 0 && y == 0))
                        continue;

                    var collider = collisionWorld[testY][testX];
                    if (collider != null && boundingBox.IsColliding(collider))
                        return collider;
                }
            }

            return null;
        }
    }
}
