﻿using System;
using YAMG.Utility;

namespace YAMG.Engine.System.Collision
{
    public class BoundingBox
    {
        public Vector Position { get; }
        public Vector Size { get; }

        public float Left => Position.X + offset.X;
        public float Top => Position.Y + offset.Y;
        public float Right => Position.X + Size.X - offset.X;
        public float Bottom => Position.Y + Size.Y - offset.X;

        private readonly Vector offset;

        public BoundingBox(float x, float y, float width, float height, float offsetX = 0, float offsetY = 0)
        {
            Position = new Vector(x, y);
            Size = new Vector(width, height);

            offset = new Vector(offsetX, offsetY);

            if (offsetX > width / 2f || offsetY > height / 2f)
                throw new ArgumentException("Offset can't be larger that bounding box");
        }

        public static bool IsColliding(BoundingBox first, BoundingBox second)
        {
            return first.Right > second.Left
                   && second.Right > first.Left
                   && first.Bottom > second.Top
                   && second.Bottom > first.Top;
        }
        public bool IsColliding(BoundingBox collider)
        {
            return IsColliding(this, collider);
        }

        public BoundingBox MoveTo(Vector position)
        {
            Position.Set(position);
            return this;
        }
        public BoundingBox MoveTo(float x, float y)
        {
            Position.Set(x, y);
            return this;
        }

        public BoundingBox Translate(Vector delta)
        {
            Position.Add(delta);
            return this;
        }
        public BoundingBox Translate(float deltaX, float deltaY)
        {
            Position.Add(deltaX, deltaY);
            return this;
        }
    }
}