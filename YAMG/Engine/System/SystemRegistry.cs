﻿using YAMG.Engine.Input;
using YAMG.Engine.System.Collision;
using YAMG.Engine.System.Fov;
using YAMG.Engine.Utility;
using YAMG.Game.Controllers;
using YAMG.Game.Utility;

namespace YAMG.Engine.System
{
    /// <summary>
    /// The object registry manages a collection of global singleton objects.  
    /// However, it differs from the standard singleton pattern in a few important ways:
    ///     - The objects managed by this class have an undefined lifetime.They may become invalid at
    ///       any time, and they may not be valid at the beginning of the program.
    ///     - The only object that is always guaranteed to be valid is the SystemRegistry itself.
    ///     - There may be more than one SystemRegistry, and there may be more than one instance of any of
    ///       the systems managed by SystemRegistry allocated at once.For example, separate threads may
    ///       maintain their own separate SystemRegistry instances.
    /// </summary>
    public class SystemRegistry
    {
        public CameraSystem Camera { get; set; }
        public FovSystem FovSystem { get; set; }
        public CollisionSystem CollisionSystem { get; set; }
        public HudSystem HudSystem { get; set; }
        public LevelSystem LevelSystem { get; set; }

        public GameController GameController { get; set; }
        public InputGameIntefrace InputGameIntefrace { get; set; }
        public GameObjectFactory GameObjectFactory { get; set; }
        public ContextParameters ContextParameters { get; set; }


        //TODO: replace with real geme object manager
        public GameRoot GameRoot { get; set; }
    }
}
