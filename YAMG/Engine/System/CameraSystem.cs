﻿using System;
using YAMG.Engine.Core;
using YAMG.Game.Constants;
using YAMG.Utility;

namespace YAMG.Engine.System
{
    public class CameraSystem : BaseObject
    {
        const float CameraFollowSpeed = 2;

        public GameObject Target { get; set; }
        public Vector FocusPosition { get; }
        public int TileX { get; private set; }
        public int TileY { get; private set; }


        private readonly Vector _target;

        public CameraSystem()
        {
            FocusPosition = new Vector();
            _target = new Vector();
        }

        public override void Reset()
        {
            FocusPosition.Zero();
            _target.Zero();
        }

        public override void Update(BaseObject parent)
        {
            if (Target == null)
                return;

            _target.Set(
                Target.CenterX - SystemComponents.ContextParameters.ViewWidth / 2f,
                Target.CenterY - SystemComponents.ContextParameters.ViewHeight / 2f);

            if (Math.Abs(_target.X - FocusPosition.X) > 600)
                FocusPosition.X = _target.X + 150;

            if (Math.Abs(_target.Y - FocusPosition.Y) > 600)
                FocusPosition.Y = _target.Y + 150;


            if (Math.Abs(_target.X - FocusPosition.X) > 40)
            {
                if (FocusPosition.X < _target.X)
                {
                    FocusPosition.X += CameraFollowSpeed;
                }
                else if (FocusPosition.X > _target.X)
                {
                    FocusPosition.X -= CameraFollowSpeed;
                }
            }

            if (Math.Abs(_target.Y - FocusPosition.Y) > 40)
            {
                if (FocusPosition.Y < _target.Y)
                {
                    FocusPosition.Y += CameraFollowSpeed;
                }
                else if (FocusPosition.Y > _target.Y)
                {
                    FocusPosition.Y -= CameraFollowSpeed;
                }
            }

            FocusPosition.Set(
                (float)Math.Round(FocusPosition.X),
                (float)Math.Round(FocusPosition.Y));

            TileX = (int)Target.CenterX / GameConstants.TileWidth;
            TileY = (int)Target.CenterY / GameConstants.TileHeight;
        }
    }
}
