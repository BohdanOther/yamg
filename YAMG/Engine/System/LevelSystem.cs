﻿using Android.Util;
using System;
using System.Collections.Generic;
using System.IO;
using YAMG.Engine.Core;
using YAMG.Engine.World;
using YAMG.Engine.World.Generator;
using YAMG.Game.Constants;
using YAMG.Game.Utility;

namespace YAMG.Engine.System
{
    public sealed class LevelSystem : BaseObject
    {
        public int WidthInTiles { get; private set; }
        public int HeightInTiles { get; private set; }
        public int TileWidth { get; private set; }
        public int TileHeight { get; private set; }

        public float LevelWidth => WidthInTiles * TileWidth;
        public float LevelHeight => HeightInTiles * TileHeight;

        public GameRoot GameRoot { get; set; }
        public GameObject BackgroundObject { get; set; }

        private TiledWorld spawnLocations;

        public LevelSystem()
        {
            Reset();
        }

        public override void Reset()
        {
            if (BackgroundObject != null && GameRoot != null)
            {
                BackgroundObject.RemoveAll();
                BackgroundObject.CommitUpdates();
                GameRoot.Remove(BackgroundObject);
                BackgroundObject = null;
                GameRoot = null;
            }

            spawnLocations = null;
        }

        public void LoadLevel(StreamReader stream, GameRoot gameRoot)
        {
            // read number of layers from stream
            // read all layers
            // and init systems

            throw new NotImplementedException();
        }

        //TODO Pattern strategy
        public void GenerateLevel(int widthInTiles, int heightInTiles, IWorldGenerator worldGenerator, GameRoot gameRoot)
        {
            var generatedWorld = worldGenerator.Generate(widthInTiles, heightInTiles);

            var collision = new int[heightInTiles][];
            var gameObjects = new int[heightInTiles][];

            // exit/entrance search and sanity checks.
            // We could've just made generator interface to provide exit and enter
            // but than we would have to check if they were in world boundaries, etc.
            // I want to keep generator as simple as possible - just provide matrix of numbers [0 - 3].
            Tuple<int, int> entrance = null;
            Tuple<int, int> exit = null;
            for (var i = 0; i < heightInTiles; i++)
            {
                collision[i] = new int[widthInTiles];
                gameObjects[i] = new int[widthInTiles];

                for (var j = 0; j < widthInTiles; j++)
                {
                    int tile = generatedWorld[i][j];
                    if (tile == 2 && entrance == null) // enter
                    {
                        entrance = new Tuple<int, int>(j, i);
                        generatedWorld[i][j] = 0;
                    }
                    else if (tile == 2 && entrance != null)
                    {
                        throw new InvalidOperationException("Generated world must have only one entrance.");
                    }

                    if (tile == 3 && exit == null) // exit
                    {
                        exit = new Tuple<int, int>(j, i);
                        generatedWorld[i][j] = 0;
                    }
                    else if (tile == 3 && exit != null)
                    {
                        throw new InvalidOperationException("Generated wolrd must have only one exit.");
                    }

                    // TODO: change this to kind of IntToCollidable(), IntToGameObject
                    // because there may be more collidable tiles that Wall
                    // and there will be a lot of game ogjects.
                    collision[i][j] = tile == 1 ? 1 : 0;
                    gameObjects[i][j] = (int)(tile == 2
                        ? GameObjectFactory.GameObjectType.Player
                        : GameObjectFactory.GameObjectType.Invalid);
                }
            }

            if (entrance == null || exit == null)
                throw new InvalidOperationException("Generated world must have exit and entrance");


            LoadLevel(new List<LevelLayer>
            {
                new LevelLayer
                {
                    Type = LevelLayerType.Background,
                    World = new TiledWorld(generatedWorld)
                },
                new LevelLayer
                {
                    // use same tiles for collision
                    // where 1 - collidable wall,
                    // 0 - empty space
                    Type = LevelLayerType.Collision,
                    World = new TiledWorld(collision)
                },
                new LevelLayer
                {
                    Type = LevelLayerType.GameObjects,
                    World =new TiledWorld(gameObjects)
                }
            },
            gameRoot);
        }


        private void LoadLevel(List<LevelLayer> levelLayers, GameRoot gameRoot)
        {
            GameRoot = gameRoot;
            TileWidth = GameConstants.TileWidth;
            TileHeight = GameConstants.TileHeight;

            // Collision always defines the world boundaries.
            var collisionLayer = levelLayers.Find(l => l.Type == LevelLayerType.Collision);
            if (collisionLayer == null)
                throw new ArgumentException("Collision layer must exist.");

            WidthInTiles = collisionLayer.World.Width;
            HeightInTiles = collisionLayer.World.Height;

            for (var i = 0; i < levelLayers.Count; i++)
            {
                var layer = levelLayers[i];
                switch (layer.Type)
                {
                    case LevelLayerType.Background:
                        var gameObjectFactory = SystemComponents.GameObjectFactory;
                        if (gameObjectFactory != null)
                        {
                            Log.Debug("LevelSystem", "Spawning world");
                            var world = gameObjectFactory.SpawnGameWorld(layer.World, TileWidth, TileHeight);
                            gameRoot.Add(new World.World(world));
                        }
                        break;

                    case LevelLayerType.Collision:
                        var collisionSystem = SystemComponents.CollisionSystem;
                        collisionSystem?.Initialize(layer.World, TileWidth, TileHeight);

                        var fov = SystemComponents.FovSystem;
                        fov?.Initialize(layer.World);
                        break;

                    case LevelLayerType.GameObjects:
                        spawnLocations = layer.World;
                        SpawnObjects();
                        break;

                    case LevelLayerType.HotSpots:
                        // TODO: implement hot spot system
                        break;
                }
            }
        }

        public void SpawnObjects()
        {
            var gameObjectFactory = SystemComponents.GameObjectFactory;
            if (gameObjectFactory != null && spawnLocations != null)
            {
                Log.Debug("LevelSystem", "Spawning Objects");
                gameObjectFactory.SpawnFromWorld(spawnLocations, TileWidth, TileHeight);
            }
        }


        private enum LevelLayerType
        {
            Background,
            Collision,
            GameObjects,
            HotSpots
        }

        private class LevelLayer
        {
            public LevelLayerType Type { get; set; }
            public TiledWorld World { get; set; }
        }
    }
}
