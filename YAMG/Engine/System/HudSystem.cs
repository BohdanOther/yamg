﻿using Android.Graphics;
using YAMG.Engine.Core;

namespace YAMG.Engine.System
{
    public class HudSystem : BaseObject
    {
        private Paint ctrl;
        private Paint txt;
        private Paint dot;
        private Paint camTile;

        public HudSystem()
        {
            ctrl = new Paint
            {
                Color = new Color(0, 0, 0, 20)
            };
            txt = new Paint
            {
                Color = new Color(255, 0, 0),
                TextSize = 30
            };
            dot = new Paint
            {
                Color = Color.MediumVioletRed
            };
            camTile = new Paint
            {
                StrokeWidth = 2,
                Color = Color.Blue
            };
        }

        public override void Reset()
        {

        }

        public override void Render(Canvas canvas)
        {
            var dpad = SystemComponents.GameController.DPad;
            var cr = SystemComponents.InputGameIntefrace.DPadRadius;
            var controller = SystemComponents.GameController;
            var cam = SystemComponents.Camera;

            // draw conroller circle around
            canvas.DrawCircle(controller.Center.X, controller.Center.Y, cr, ctrl);

            // draw circle of current dpad delta
            if (dpad.IsPressed)
                canvas.DrawCircle(
                    controller.Center.X + dpad.X * cr + 2,
                    controller.Center.Y + dpad.Y * cr + 1,
                    10,
                    dot);

            canvas.DrawText($"{dpad.X:f1}; {dpad.Y:f1}", 20, 620, txt);
        }
    }
}
