﻿using System.Diagnostics;

namespace YAMG.Engine.System.Fov
{
    public class ShadowCaster
    {
        public static void ComputeVisibility(LightMap grid, int fromX, int fromY, float viewRadius)
        {
            Debug.Assert(fromX >= 0 && fromX < grid.Width);
            Debug.Assert(fromY >= 0 && fromY < grid.Height);

            // Viewer's cell is always visible.
            grid.SetLight(fromX, fromY);

            float vrs = viewRadius * viewRadius;

            Scan1(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan2(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan3(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan4(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan5(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan6(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan7(grid, fromX, fromY, vrs, 1, 1, 0);
            Scan8(grid, fromX, fromY, vrs, 1, 1, 0);
        }


        private static void Scan1(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int y = fromY - pDepth;
            if (y < 0) return;

            int x = fromX - (int)(pStartSlope * pDepth);
            if (x < 0) x = 0;

            while (GetSlope(x, y, fromX, fromY, false) >= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (x - 1 >= 0 && !map.IsWall(x - 1, y))
                            Scan1(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x - 0.5, y + 0.5, fromX, fromY, false));
                    }
                    else
                    {
                        if (x - 1 >= 0 && map.IsWall(x - 1, y))
                            pStartSlope = GetSlope(x - 0.5, y - 0.5, fromX, fromY, false);

                        map.SetLight(x, y);
                    }
                }
                x++;
            }
            x--;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan1(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }

        private static void Scan2(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int y = fromY - pDepth;
            if (y < 0) return;

            int x = fromX + (int)(pStartSlope * pDepth);
            if (x >= map.Width) x = map.Width - 1;

            while (GetSlope(x, y, fromX, fromY, false) <= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (x + 1 < map.Width && !map.IsWall(x + 1, y))
                            Scan2(map, fromX, fromY, rad,
                                 pDepth + 1, pStartSlope, GetSlope(x + 0.5, y + 0.5, fromX, fromY, false));
                    }
                    else
                    {
                        if (x + 1 < map.Width && map.IsWall(x + 1, y))
                            pStartSlope = -GetSlope(x + 0.5, y - 0.5, fromX, fromY, false);

                        map.SetLight(x, y);
                    }
                }
                x--;
            }
            x++;
            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan2(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }
        private static void Scan3(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = fromX + pDepth;
            if (x >= map.Width) return;

            int y = fromY - (int)(pStartSlope * pDepth);
            if (y < 0) y = 0;

            while (GetSlope(x, y, fromX, fromY, true) <= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (y - 1 >= 0 && !map.IsWall(x, y - 1))
                            Scan3(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x - 0.5, y - 0.5, fromX, fromY, true));
                    }
                    else
                    {
                        if (y - 1 >= 0 && map.IsWall(x, y - 1))
                            pStartSlope = -GetSlope(x + 0.5, y - 0.5, fromX, fromY, true);

                        map.SetLight(x, y);
                    }
                }
                y++;
            }
            y--;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan3(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }

        private static void Scan4(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = fromX + pDepth;
            if (x >= map.Width) return;

            int y = fromY + (int)(pStartSlope * pDepth);
            if (y >= map.Height) y = map.Height - 1;

            while (GetSlope(x, y, fromX, fromY, true) >= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (y + 1 < map.Height && !map.IsWall(x, y + 1))
                            Scan4(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x - 0.5, y + 0.5, fromX, fromY, true));
                    }
                    else
                    {
                        if (y + 1 < map.Height && map.IsWall(x, y + 1))
                            pStartSlope = GetSlope(x + 0.5, y + 0.5, fromX, fromY, true);

                        map.SetLight(x, y);
                    }
                }
                y--;
            }
            y++;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan4(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }

        private static void Scan5(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int y = fromY + pDepth;
            if (y >= map.Height) return;

            int x = fromX + (int)(pStartSlope * pDepth);
            if (x >= map.Width) x = map.Width - 1;

            while (GetSlope(x, y, fromX, fromY, false) >= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (x + 1 < map.Width && !map.IsWall(x + 1, y))
                            Scan5(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x + 0.5, y - 0.5, fromX, fromY, false));
                    }
                    else
                    {
                        if (x + 1 < map.Width && map.IsWall(x + 1, y))
                            pStartSlope = GetSlope(x + 0.5, y + 0.5, fromX, fromY, false);

                        map.SetLight(x, y);
                    }
                }
                x--;
            }
            x++;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan5(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }

        private static void Scan6(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int y = fromY + pDepth;
            if (y >= map.Height) return;

            int x = fromX - (int)(pStartSlope * pDepth);
            if (x < 0) x = 0;

            while (GetSlope(x, y, fromX, fromY, false) <= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (x - 1 >= 0 && !map.IsWall(x - 1, y))
                            Scan6(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x - 0.5, y - 0.5, fromX, fromY, false));
                    }
                    else
                    {
                        if (x - 1 >= 0 && map.IsWall(x - 1, y))
                            pStartSlope = -GetSlope(x - 0.5, y + 0.5, fromX, fromY, false);

                        map.SetLight(x, y);
                    }
                }
                x++;
            }
            x--;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan6(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }

        private static void Scan7(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = fromX - pDepth;
            if (x < 0) return;

            int y = fromY + (int)(pStartSlope * pDepth);
            if (y >= map.Height) y = map.Height - 1;

            while (GetSlope(x, y, fromX, fromY, true) <= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (y + 1 < map.Height && !map.IsWall(x, y + 1))
                            Scan7(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x + 0.5, y + 0.5, fromX, fromY, true));
                    }
                    else
                    {
                        if (y + 1 < map.Height && map.IsWall(x, y + 1))
                            pStartSlope = -GetSlope(x - 0.5, y + 0.5, fromX, fromY, true);

                        map.SetLight(x, y);
                    }
                }
                y--;
            }
            y++;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan7(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }

        private static void Scan8(LightMap map, int fromX, int fromY, float rad,
            int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = fromX - pDepth;
            if (x < 0) return;

            int y = fromY - (int)(pStartSlope * pDepth);
            if (y < 0) y = 0;

            while (GetSlope(x, y, fromX, fromY, true) >= pEndSlope)
            {
                if (GetVisDistance(x, y, fromX, fromY) <= rad)
                {
                    if (map.IsWall(x, y))
                    {
                        if (y - 1 >= 0 && !map.IsWall(x, y - 1))
                            Scan8(map, fromX, fromY, rad,
                                pDepth + 1, pStartSlope, GetSlope(x + 0.5, y - 0.5, fromX, fromY, true));
                    }
                    else
                    {
                        if (y - 1 >= 0 && map.IsWall(x, y - 1))
                            pStartSlope = GetSlope(x - 0.5, y - 0.5, fromX, fromY, true);

                        map.SetLight(x, y);
                    }
                }
                y++;
            }
            y--;

            if (KeepRecursion(map, rad, x, y, pDepth))
                Scan8(map, fromX, fromY, rad, pDepth + 1, pStartSlope, pEndSlope);
        }


        private static bool KeepRecursion(LightMap map, float rad, int x, int y, int depth)
        {
            if (x < 0) x = 0;
            else if (x >= map.Width) x = map.Width - 1;

            if (y < 0) y = 0;
            else if (y >= map.Height) y = map.Height - 1;

            return (depth < rad && !map.IsWall(x, y));
        }

        private static double GetSlope(double pX1, double pY1, double pX2, double pY2, bool pInvert)
        {
            if (pInvert)
                return (pY1 - pY2) / (pX1 - pX2);

            return (pX1 - pX2) / (pY1 - pY2);
        }

        private static int GetVisDistance(int pX1, int pY1, int pX2, int pY2)
        {
            return (pX1 - pX2) * (pX1 - pX2) + (pY1 - pY2) * (pY1 - pY2);
        }
    }
}
