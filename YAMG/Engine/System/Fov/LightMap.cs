﻿namespace YAMG.Engine.System.Fov
{
    public enum LightTile
    {
        /// <summary>
        /// Lightmap tile is lighten.
        /// </summary>
        Bright,

        /// <summary>
        /// Lightmap tile in shadow.
        /// </summary>
        Dark,

        /// <summary>
        /// Lightmap tile blocks any light.
        /// </summary>
        Blocker
    }

    public class LightMap
    {
        public int Width { get; }
        public int Height { get; }

        private readonly int[][] walls;
        private readonly LightTile[][] light;

        private static readonly int[][] Directions = {
            //      x   y
            new[] { 0, -1 }, // up
            new[] { 0,  1 }, // down
            new[] {-1,  0 }, // left
            new[] { 1,  0 }, // right
            new[] {-1, -1 }, // up-left
            new[] { 1, -1 }, // up-right
            new[] {-1,  1 }, // down-left
            new[] { 1,  1 }  // down-right 
        };

        public LightMap(int[][] walls)
        {
            this.walls = walls;

            Height = walls.GetLength(0);
            Width = walls[0].Length;

            light = new LightTile[Height][];
            for (int i = 0; i < Height; i++)
            {
                light[i] = new LightTile[Width];
                for (int j = 0; j < Width; j++)
                {
                    light[i][j] = LightTile.Dark;
                }
            }
        }

        public LightTile GetLight(int gridX, int gridY)
        {
            return light[gridY][gridX];
        }

        public void SetLight(int gridX, int gridY)
        {
            light[gridY][gridX] = LightTile.Bright;
            for (int i = 0; i < Directions.Length; i++)
            {
                int dx = gridX + Directions[i][0];
                int dy = gridY + Directions[i][1];
                if (dx >= 0 && dy >= 0 && dx < Width && dy < Height && IsWall(dx, dy))
                    light[dy][dx] = LightTile.Blocker;
            }
        }

        public void ClearLight()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    light[i][j] = LightTile.Dark;
                }
            }
        }

        public bool IsWall(int x, int y)
        {
            // TODO: int.IsWall
            return walls[y][x] == 1;
        }

        public LightTile this[int gridX, int gridY] => light[gridY][gridX];
    }
}