﻿using Android.Graphics;
using YAMG.Engine.Core;
using YAMG.Engine.World;

namespace YAMG.Engine.System.Fov
{
    public sealed class FovSystem : BaseObject
    {
        public LightMap LightMap { get; private set; }

        private int tileX;
        private int tileY;

        private float fovRad = 8f;

        private Paint p;
        private Paint pBlocker;

        public FovSystem()
        {
            p = new Paint
            {
                Color = new Color(255, 255, 255, 100)
            };

            pBlocker = new Paint
            {
                Color = new Color(255, 0, 0)
            };
            Reset();
        }

        public void Initialize(TiledWorld world)
        {
            LightMap = new LightMap(world.Tiles);
        }

        public override void Reset()
        {
            tileX = 0;
            tileY = 0;
        }

        public override void Update(BaseObject parent)
        {
            // if position hasn't changed, no update required
            var cam = SystemComponents.Camera;
            if (tileX == cam.TileX && tileY == cam.TileY)
                return;

            tileX = cam.TileX;
            tileY = cam.TileY;

            LightMap.ClearLight();
            ShadowCaster.ComputeVisibility(LightMap, tileX, tileY, fovRad);
        }

        //public override void Render(Canvas canvas)
        //{
        //    for (int y = 0; y < LightMap.Height; y++)
        //    {
        //        for (int x = 0; x < LightMap.Width; x++)
        //        {
        //          //  if (LightMap[x, y] == LightTile.Bright)
        //           // {
        //          //      canvas.DrawRect(x * 32, y * 32, x * 32 + 32, y * 32 + 32, p);
        //          //  }
        //            if (LightMap[x, y] == LightTile.Blocker)
        //            {
        //                canvas.DrawRect(x * 32-2, y * 32-2, x * 32 + 34, y * 32 + 34, pBlocker);
        //            }
        //        }
        //    }
        //}
    }
}
