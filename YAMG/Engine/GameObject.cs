﻿using YAMG.Engine.Core;
using YAMG.Utility;

namespace YAMG.Engine
{
    /// <summary>
    /// GameObject defines any object that resides in the game world 
    /// (character, background, special effect, enemy, etc).  
    /// 
    /// It is a collection of GameComponents which implement its behavior; 
    /// GameObjects themselves have no intrinsic behavior.  
    /// 
    /// GameObjects are also "bags of data" that components can use to share state 
    /// (direct component-to-component communication is discouraged).
    /// </summary>
    public sealed class GameObject : PhasedObjectManager
    {
        // These fields are managed by components
        public Vector Position { get; set; }
        public Vector Velocity { get; set; }
        public Vector TargetVelocity { get; set; }
        public Vector Acceleration { get; set; }

        public float Width { get; set; }
        public float Heigth { get; set; }

        public float CenterX => Position.X + Width / 2f;
        public float CenterY => Position.Y + Heigth / 2f;

        // TODO: use for state automata
        //public enum ActionType
        //{
        //    Invalid,
        //    Idle,
        //    Move,
        //    HitReact,
        //    Death
        //    //...
        //}
        //private ActionType CurrentAction { get; set; }

        public GameObject()
        {
            Position = new Vector();
            Velocity = new Vector();
            TargetVelocity = new Vector();
            Acceleration = new Vector();

            Width = 0;
            Heigth = 0;

            Reset();
        }

        public override void Reset()
        {
            RemoveAll();
            CommitUpdates();

            Position.Zero();
            Velocity.Zero();
        }
    }
}
