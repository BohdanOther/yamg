using System;
using System.Threading;

namespace YAMG.Engine.Threading
{
    public class UpdateThread : YamgThread
    {
        private const int TargetUps = 60;

        public GameRoot GameRoot { private get; set; }

        // If we're slow, then we want to skip some draw-frames and update.
        // This sets max amount of updates
        // without rendering the frame.
        const int MaxRenderSkips = 5;

        public UpdateThread() : base(TargetUps) { }

        public override void Run()
        {
            Elapsed = 0;

            int updates;
            long lastUpdateTime = Environment.TickCount;

            while (IsRunning)
            {
                updates = 0;
                Elapsed = Environment.TickCount - lastUpdateTime;

                RequestSleep();

                while (Environment.TickCount - lastUpdateTime > UpdateDeltaTime && updates < MaxRenderSkips)
                {
                    GameRoot.Update(null);
                    lastUpdateTime += UpdateDeltaTime;
                    updates++;
                }

                if (IsPaused)
                {
                    while (IsPaused)
                    {
                        lock (PauseLock)
                        {
                            Monitor.Wait(PauseLock);
                        }
                    }
                    lastUpdateTime = Environment.TickCount;
                }
            }
        }
    }
}