using System;
using System.Threading;
using YAMG.Engine.Drawing;

namespace YAMG.Engine.Threading
{
    public class DrawThread : YamgThread
    {
        private const int TargetFps = 50;

        private readonly IGameView gameView;
        public DrawThread(IGameView gameView) : base(TargetFps)
        {
            this.gameView = gameView;
        }

        public override void Run()
        {
            Elapsed = 0;

            long currentTimeMillis;
            long lastUpdateTime = Environment.TickCount;

            while (IsRunning)
            {
                currentTimeMillis = Environment.TickCount;
                Elapsed = currentTimeMillis - lastUpdateTime;

                if (IsPaused)
                {
                    while (IsPaused)
                    {
                        lock (PauseLock)
                        {
                            Monitor.Wait(PauseLock);
                        }
                    }
                    currentTimeMillis = Environment.TickCount;
                }

                RequestSleep();

                gameView.Draw();
                lastUpdateTime = currentTimeMillis;
            }
        }
    }
}