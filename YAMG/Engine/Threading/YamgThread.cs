﻿using System.Threading;
using Java.Lang;

namespace YAMG.Engine.Threading
{
    public abstract class YamgThread : Thread
    {
        /// <summary>
        /// Generally, how many time you want to run
        /// per second.
        /// </summary>
        protected readonly int UpdatesPerSecond;

        /// <summary>
        /// 1000 / UPS, number of miliseconds
        /// between updates.
        /// </summary>
        protected readonly int UpdateDeltaTime;

        protected readonly object PauseLock = new object();

        /// <summary>
        /// Miliseconds sinced last update.
        /// </summary>
        protected long Elapsed;

        public bool IsRunning { get; private set; } = true;
        public bool IsPaused { get; private set; }


        protected YamgThread(int updatesPerSecond)
        {
            UpdatesPerSecond = updatesPerSecond;
            UpdateDeltaTime = 1000 / updatesPerSecond;
        }

        public override void Start()
        {
            IsRunning = true;
            IsPaused = false;
            base.Start();
        }

        public void StopGame()
        {
            IsRunning = false;
            ResumeGame();
        }

        public void PauseGame()
        {
            IsPaused = true;
        }

        public void ResumeGame()
        {
            if (IsPaused)
            {
                IsPaused = false;
                lock (PauseLock)
                {
                    Monitor.Pulse(PauseLock);
                }
            }
        }

        protected virtual void RequestSleep()
        {
            if (Elapsed < UpdateDeltaTime)
            {
                // This locks UPS/FPS around UpdatesPerSecond.
                // (Which should be under 60;
                // basically, we want to save device battery
                // and not render or update as fast as we can)
                try
                {
                    Sleep(UpdateDeltaTime - Elapsed);
                }
                catch (InterruptedException)
                {
                    // It's ok, ignore
                }
            }
        }

        // force derived classes to provide own implementation
        public abstract override void Run();
    }
}
