﻿using Android.Util;
using YAMG.Engine.Core;

namespace YAMG.Engine
{
    /// <summary>
    /// Main game root. This object is effectively the root of the game graph.
    /// It updates all its child nodes.
    /// </summary>
    /// <remarks>
    /// While this object actually does nothing
    /// I want to emphasis on fact that this is Root of game graph
    /// and manages ALL game objects and engine systems.
    /// </remarks>
    public sealed class GameRoot : ObjectManager
    {
        public GameRoot()
        {
        }

        public override void Update(BaseObject parent)
        {
            Log.Debug("Game Root", "Updating game root");
            base.Update(parent);
        }
    }
}
